CREATE TABLE `app_user` (
`id` varchar(255) NOT NULL,
`email` varchar(255) DEFAULT NULL,
`first_name` varchar(255) DEFAULT NULL,
`last_name` varchar(255) DEFAULT NULL,
`locked` bit(1) DEFAULT NULL,
`login` varchar(255) DEFAULT NULL,
`middle_name` varchar(255) DEFAULT NULL,
`password_hash` varchar(255) DEFAULT NULL,
`role` varchar(255) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `app_session` (
`id` varchar(255) NOT NULL,
`signature` varchar(255) DEFAULT NULL,
`timestamp` bigint(20) DEFAULT NULL,
`user_id` varchar(255) DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `FKrrdhu5ryvprfqplat774p2n4t` (`user_id`),
CONSTRAINT `FKrrdhu5ryvprfqplat774p2n4t` FOREIGN KEY (`user_id`)
REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `app_project` (
`id` varchar(255) NOT NULL,
`start_date` datetime DEFAULT NULL,
`finish_date` datetime DEFAULT NULL,
`description` varchar(255) DEFAULT NULL,
`name` varchar(255) DEFAULT NULL,
`user_id` varchar(255) DEFAULT NULL,
`status` varchar(255) NOT NULL DEFAULT 'NOT_STARTED',
`created` datetime DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `FKp9byv3k2r7rgg7svn3rx10a1u` (`user_id`),
CONSTRAINT `FKp9byv3k2r7rgg7svn3rx10a1u` FOREIGN KEY (`user_id`)
REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `app_task` (
`id` varchar(255) NOT NULL,
`start_date` datetime DEFAULT NULL,
`finish_date` datetime DEFAULT NULL,
`description` varchar(255) DEFAULT NULL,
`name` varchar(255) DEFAULT NULL,
`project_id` varchar(255) DEFAULT NULL,
`user_id` varchar(255) DEFAULT NULL,
`status` varchar(255) NOT NULL DEFAULT 'NOT_STARTED',
`created` datetime DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `FKsu3pcsyuwrs6nmpcpufikq5u4` (`project_id`),
KEY `FKkc5pwubxw7j4b0xprgdmgkrel` (`user_id`),
CONSTRAINT `FKkc5pwubxw7j4b0xprgdmgkrel` FOREIGN KEY (`user_id`)
REFERENCES `app_user` (`id`),
CONSTRAINT `FKsu3pcsyuwrs6nmpcpufikq5u4` FOREIGN KEY (`project_id`)
REFERENCES `app_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;