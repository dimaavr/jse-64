package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.avramenko.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.avramenko.tm.api.service.dto.ISessionDtoService;
import ru.tsc.avramenko.tm.api.service.dto.IUserDtoService;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.UserDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @Autowired
    private ISessionDtoService sessionDtoService;

    @Autowired
    private IUserDtoService userService;

    @Override
    @Nullable
    @WebMethod
    public UserDTO lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        return userService.lockUserByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        userService.removeById(id);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        return userService.unlockUserByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO setUserRole(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "userId", partName = "userId") @NotNull final String id,
            @WebParam(name = "role", partName = "role") @NotNull final Role role
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        return userService.setRole(id, role);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO updateUserById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        return userService.updateUserById(session.getUserId(), firstName, lastName, middleName, email);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO updateUserByLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        return userService.updateUserByLogin(login, firstName, lastName, middleName, email);
    }

    @Override
    @Nullable
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        sessionDtoService.validate(session);
        userService.setPassword(session.getUserId(), password);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findUserByLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        return userService.findByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findUserById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        return userService.findById(id);
    }

    @NotNull
    @Override
    @WebMethod
    public void clearUser(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        userService.clear();
    }

    @Nullable
    @Override
    @WebMethod
    public List<UserDTO> findAllUser(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionDtoService.validate(session, Role.ADMIN);
        return userService.findAll();
    }

}